import { observable, action } from "mobx";
import axios from 'axios'
import REST from '../config/api'
export default class FeedbackStore{
      
    mock = {
        eventId:'id', 
        text:'EventName',
        categories:[
            "organisation",
            "preis"        
        ]
    }

    @observable
    text_field = ""

    @observable
    feedbacks = []
    
    @observable
    activeSurvey = {}

    constructor(root, mode){
        this.root = root;
        this.API = REST.feedbackAPI(mode)
        this.feedbacks = []
    }

    @action
    async getFeedbacks(){
        return new Promise(async (resolve,reject)=> {
            try {
                let surveys = await axios.get(this.API.FEEDBACK_GET_ACTIVE)

                this.feedbacks = surveys.data.Items
                resolve() 
            }catch(e){
                console.log(e)
                reject()
            }
        })
    }

    @action
    async submitFeedback(){
        return new Promise(async (resolve,reject)=> {            
            try {
            let surveys = await axios.post(this.API.FEEDBACK_SUBMIT,this.activeSurvey)
                resolve()
            }catch(e){
                console.log(e)
            }
        })
    }




   
  
       
}


