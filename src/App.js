import React from 'react';
import Amplify from 'aws-amplify';
import {MemoryStorageNew} from './store/MemoryStorageNew'

Amplify.configure({
  Auth:{
    region: "eu-central-1",    
    userPoolId: "eu-central-1_IiyfsyTSB",
    userPoolWebClientId: "557rqotcq85a3npo2tc1iluphd",
    storage: MemoryStorageNew
  }
});

import { Platform, StatusBar, StyleSheet, View} from 'react-native';
import { AppLoading } from 'expo';
import axios from 'axios'
import {Auth} from 'aws-amplify'
import {store} from './store/store'
import {  SplashScreen } from 'expo';
import {observer} from 'mobx-react'
import { Root } from 'native-base';
import AppNavigator from './navigation/AppNavigator';

axios.interceptors.request.use(function (config) {
  return Auth.currentSession()
    .then(session => {
      // User is logged in. Set auth header on all requests
      config.headers.Authorization = 'Bearer ' + session.getIdToken().getJwtToken()
      return Promise.resolve(config)
    })
    .catch(() => {
      // No logged-in user: don't set auth header
      return Promise.resolve(config)
    })
})

@observer
export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };  

  async componentDidMount(){
    try {
      await store.authStore.checkCurrentUser()
    }catch(e){
      console.log(e)
    }
   
    this.setState({isLoadingComplete:true})
  }
 
  render() {
    console.log("MAIN: mounted")

    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading         
          startAsync={this._hide}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
          autoHideSplash={false}
        /> 
      );
    } else {     
        //false
           
      return (
        <View style={styles.container}>        
          <Root>
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}             
               <AppNavigator />
          </Root>        
        </View>
      );
    }
  }

  _hide = async () => {    
    SplashScreen.hide();
  }
 
  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#400d5d',
  },
});
