import React from 'react';
import { Form,Textarea, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, Toast } from 'native-base';
import Markdown from 'react-native-markdown-renderer';
import {
   
    ScrollView,
    StyleSheet,
    KeyboardAvoidingView,   
   
  } from 'react-native';
import {store} from '../../store/store'
import { observer } from 'mobx-react';
import aws_exports from '../../store/amplify_config';
import Star from './Star'
import { CheckBox } from 'react-native-elements';

@observer
export default class FeedbackAbgabe extends React.Component {
  static navigationOptions = {
    title: 'Feedback',
  };

  constructor(props){
    super(props);
    this.state = {text_field:''}
   
  }


  async componentWillMount(){

  }

 

  
  

  render() {

    survey = store.feedbackStore.activeSurvey
    return (
      
        <ScrollView style={styles.container}>
         <Card>
                   
                <Textarea rowSpan={5} bordered placeholder="write something you wish for the next time..." value={this.state.text_field} onChangeText={(text_field => {this.setState({text_field})})}/>                      
                     
          </Card>
          {survey.categories.map((category,i)=> {
                return (
                    <Card key={i}>  
                        <CardItem header >
                        <Left>              
                            <Star category={category}></Star>
                        </Left>                       
                        </CardItem>       
                    
                    </Card>

                )
          })}  
         <Card>
         <CardItem footer>
              <Button onPress={async ()=> {
                try {
                  store.feedbackStore.activeSurvey.text_field = this.state.text_field
                  await store.feedbackStore.submitFeedback()
                  Toast.show({
                    text:"Feedback submitted! Thank you!"
                  })
                                
                 
                }catch(e){
                  
                }
              }}>
                <Text>Submit Feedback</Text>
              </Button>
            </CardItem>
         </Card>
         
          </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  card:{
    padding:10
  }

});