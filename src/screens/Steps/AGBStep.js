import React from 'react';
import { ScrollView, StyleSheet , View, Text, Picker, Item, Button} from 'react-native';
import  ProfilHead  from '../../components/ProfilePage/ProfilHead';
import {store} from '../../store/store'
import { Container, Content, Subtitle, Form, Icon } from 'native-base';
import Markdown from 'react-native-markdown-renderer';


export default class AGBStep extends React.Component {

  constructor(props){
      super(props);
      this.state= {
        section : 'german'
      }
  }
   

  render() {
    return (      
        <Container style={styles.container}> 
            <Content contentContainerStyle={styles.container}>
              <ScrollView style={{backgroundColor:'#fff'}}>
                <Markdown> {legal}</Markdown>
              </ScrollView>     
                
                <Button 
                    style={styles.button}
                    title='Accept Data Policy'
                    onPress={async ()=> {
                        console.log("accept")
                        this.props.navigation.navigate('App')
                    }}
                /> 
            </Content>              
        </Container>
    );
  } 
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#400d5d', 
    alignItems:'center',
    justifyContent:'center'   
    
  },
  header:{
      color:'#fff',
      fontSize:30,
      fontWeight:'bold'
  },
  subtitle:{
    color:'#fff',
    fontSize:15,
    fontWeight:'bold'
  },
  button:{
      marginTop:50,
      paddingTop:50
  }
});


const legal = `## Faranto e.V. Privacy Policy

*scroll down and accept*

Your rights of data subjects

Unter den angegebenen Kontaktdaten unseres Datenschutzbeauftragten können Sie jederzeit folgende Rechte ausüben:

 

You can exercise the following rights at any time using the contact details provided by our data protection officer:

 

Information about your data stored with us and their processing (Art. 15 DSGVO),

Correction of incorrect personal data (Art. 16 DSGVO),

Deletion of your data stored with us (Art. 17 DSGVO),

Restriction of data processing if we are not yet allowed to delete your data due to legal obligations (Art. 18 DSGVO),

Objection to the processing of your data by us (Art. 21 DSGVO) and

Data transferability if you have consented to data processing or have concluded a contract with us (Art. 20 DSGVO).

If you have given us your consent, you can revoke it at any time with effect for the future.

 

You may at any time file a complaint with a supervisory authority, e.g. the competent supervisory authority of the federal state in which you reside or the authority responsible for us.

 

You will find a list of the supervisory authorities (for the non-public sector) with their addresses at: https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html.

 

Collection of general information when you visit our website

If you access our website, i.e. if you do not register or otherwise submit information, information of a general nature is automatically collected. This information (server log files) includes, for example, the type of web browser, the operating system used, the domain name of your Internet service provider, your IP address and the like.

 

In particular, they are processed for the following purposes:

 

Ensuring a trouble-free connection to the website,

Ensuring the smooth use of our website,

Evaluation of system safety and stability as well as

for other administrative purposes.

We do not use your data to draw conclusions about your person. Information of this kind is evaluated statistically by us if necessary, in order to optimize our Internet appearance and the technology behind it.

 

**Legal basis:**

Processing takes place in accordance with Art. 6 Para. 1 lit. f DSGVO on the basis of our legitimate interest in improving the stability and functionality of our website.

 

**Recipient:**

 

Recipients of the data may be technical service providers who act as contract processors for the operation and maintenance of our website.

 

**Storage period:**

 

The data is deleted as soon as it is no longer required for the purpose of collection. This is generally the case for the data used to provide the website when the respective session has ended.

 

**Provision required or required:**

 

The provision of the aforementioned personal data is neither required by law nor by contract. Without the IP address, however, the service and functionality of our website cannot be guaranteed. In addition, individual services may not be available or restricted. For this reason an objection is excluded.

 

**Cookies**

the nature and purpose of the processing:

 

Like many other websites, we also use so-called "cookies". Cookies are small text files that are stored on your end device (laptop, tablet, smartphone, etc.) when you visit our website.

 

This gives us certain data such as IP address, browser used and operating system.

 

Cookies cannot be used to start programs or transmit viruses to a computer. The information contained in cookies enables us to make navigation easier for you and to display our web pages correctly.

 

Under no circumstances will the data collected by us be passed on to third parties or linked to personal data without your consent.

 

Of course, you can also view our website without cookies. Internet browsers are regularly set to accept cookies. In general, you can deactivate the use of cookies at any time via the settings of your browser. Please use the help functions of your Internet browser to find out how you can change these settings. Please note that individual functions of our website may not work if you have deactivated the use of cookies.

 

**Storage time and cookies used:**

 

If you allow us to use cookies through your browser settings or consent, the following cookies may be used on our websites:

 

FARANTO_ID, storage duration:1 hour, https://faranto.eu-de.mybluemix.net/

 

Insofar as these cookies may (also) concern personal data, we will inform you of this in the following sections.

 

You can delete individual cookies or the entire cookie stock via your browser settings. In addition, you will receive information and instructions on how these cookies can be deleted or their storage blocked in advance. Depending on the provider of your browser, you will find the necessary information under the following links:

 

Mozilla Firefox: https://support.mozilla.org/de/kb/cookies-loeschen-daten-von-websites-entfernen

Internet Explorer: https://support.microsoft.com/de-de/help/17442/windows-internet-explorer-delete-manage-cookies

Google Chrome: https://support.google.com/accounts/answer/61416?hl=en

Opera: http://www.opera.com/de/help

Safari: https://support.apple.com/kb/PH17191?locale=en_DE&viewlocale=en_DE

Registration on our website

the nature and purpose of the processing:

 

When registering for the use of our personalised services, some personal data is collected, such as name, address, contact and communication data (e.g. telephone number and e-mail address). If you are registered with us, you can access content and services that we only offer to registered users. Registered users also have the option of changing or deleting the data provided during registration at any time. Of course, we will also provide you with information about the personal data we have stored about you at any time.

 

**Legal basis:**

 

The data entered during registration is processed on the basis of the user's consent (Art. 6 para. 1 lit. a DSGVO).

 

If the purpose of registration is to fulfil a contract to which the person concerned is a party or to carry out pre-contractual measures, the additional legal basis for processing the data is Art. 6 para. 1 lit. b DSGVO.

 

**Recipient:**

 

Recipients of the data may be technical service providers who act as contract processors for the operation and maintenance of our website.

 

**Storage period:**

 

Data will only be processed in this context as long as the corresponding consent has been given. Afterwards they will be deleted, as long as there are no legal obligations to store them. To contact us in this context, please use the contact data provided at the end of this data protection declaration.

 

**Provision prescribed or required:**

 

The provision of your personal data is voluntary, solely on the basis of your consent. Without the provision of your personal data we cannot grant you access to our offered contents and services.

 

Use of script libraries (Google Web Fonts)

the nature and purpose of the processing:

 

In order to display our content correctly and graphically appealing across browsers, we use "Google Web Fonts" from Google LLC (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; hereinafter "Google") on this website to display fonts.

 

The privacy policy of the library operator Google can be found here: https://www.google.com/policies/privacy/

 

**Legal basis:**

 

The legal basis for the integration of Google Webfonts and the associated data transfer to Google is your consent (Art. 6 para. 1 lit. a DSGVO).

 

**Recipient:**

 

Calling script libraries or font libraries automatically triggers a connection to the library operator. It is theoretically possible - but currently unclear whether and, if so, for what purposes - for the operator to collect Google data in this case.

 

**Storage duration:**

 

We do not collect any personal data through the integration of Google Web Fonts.

 

Further information on Google Web Fonts can be found at https://developers.google.com/fonts/faq and in Google's privacy policy: https://www.google.com/policies/privacy/.

 

**Third Country Transfer:**

 

Google processes your data in the USA and has submitted to the EU_US Privacy Shield https://www.privacyshield.gov/EU-US-Framework.

 

**Provision required or required:**

 

The provision of personal data is neither required by law nor by contract. However, without the correct representation of the contents of standard fonts cannot be made possible.

 

**Revocation of consent:**

 

The programming language JavaScript is regularly used to display the contents. You can therefore object to data processing by deactivating the execution of JavaScript in your browser or by installing an integration JavaScript blocker. Please note that this may result in functional restrictions on the website.

 

**Use of Google Maps**

the nature and purpose of the processing:

 

On this website we use the offer of Google Maps. Google Maps is operated by Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA (hereinafter "Google"). This enables us to display interactive maps directly on the website and enables you to use the map function conveniently.

 

For more information about data processing by Google, please refer to the Google Privacy Notice. There you can also change your personal data protection settings in the data protection centre.

 

Detailed instructions on how to manage your own data in connection with Google products can be found here.

 

Legal basis:

 

The legal basis for the integration of Google Maps and the associated data transfer to Google is your consent (Art. 6 para. 1 lit. a DSGVO).

 

Recipient:

 

When you visit the website, Google receives information that you have accessed the corresponding subpage of our website. This occurs regardless of whether Google provides a user account that you are logged in to or whether there is no user account. If you are logged into Google, your information will be directly associated with your account.

 

If you do not want your data to be assigned to your Google profile, you must log out of Google before activating the button. Google stores your data as user profiles and uses them for advertising, market research and/or for the design of its website to meet your needs. Such evaluation is carried out in particular (even for users who are not logged in) to provide demand-oriented advertising and to inform other users of the social network about your activities on our website. You have the right to object to the creation of these user profiles, and you must contact Google to exercise this right.

 

Storage period:

 

We do not collect any personal data through the integration of Google Maps.

 

Third Country Transfer:

 

Google processes your data in the USA and has submitted to the EU_US Privacy Shield https://www.privacyshield.gov/EU-US-Framework.

 

Revocation of consent:

 

If you do not want Google to collect, process or use data about you via our website, you can deactivate JavaScript in your browser settings. In this case, however, you cannot use our website or can only use it to a limited extent.

 

Provision required or required:

 

The provision of your personal data is voluntary, solely on the basis of your consent. If you prevent access, this may result in functional restrictions on the website.

 

SSL Encryption

 

In order to protect the security of your data during transmission, we use state-of-the-art encryption procedures (e.g. SSL) via HTTPS.

 

Changes to our data protection regulations

We reserve the right to adapt this data protection declaration so that it always corresponds to the current legal requirements or to implement changes to our services in the data protection declaration, e.g. when introducing new services. Your renewed visit will then be subject to the new data protection declaration.

 

Questions to the data protection officer

 

If you have any questions about data protection, please send us an e-mail or contact the person responsible for data protection in our organisation directly:

 

The data protection declaration was created with the data protection declaration generator of activeMind AG (version 2018-09-24).`