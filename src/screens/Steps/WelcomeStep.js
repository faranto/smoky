import React from 'react';
import { ScrollView, StyleSheet , View, Text, Picker, Item, Button} from 'react-native';
import  ProfilHead  from '../../components/ProfilePage/ProfilHead';
import {store} from '../../store/store'
import { Container, Content, Subtitle, Form, Icon } from 'native-base';


export default class WelcomeStep extends React.Component {

  constructor(props){
      super(props);
      this.state= {
        section : 'htw_dresden'
      }
  }
  

  render() {
    return (      
        <Container style={styles.container}> 
            <Content contentContainerStyle={styles.container}>
                <Text style={styles.header} >Welcome to Smoky </Text>
                <Text style={styles.subtitle}>Please choose a section you'd like to join!</Text>
                <Picker
                    selectedValue={this.state.section}
                    style={{ width: '100%',  fontSize:'15', backgroundColor:'#fff'}}
                    onValueChange={(itemValue, itemIndex) =>
                        this.setState({section: itemValue})
                    }>
                    <Picker.Item label="HTW - Dresden" value="htw_dresden" />
                </Picker>
                
                <Button 
                    style={styles.button}
                    title='Join Section'
                    onPress={async ()=> {
                                          
                        store.sectionStore.hasSection = this.state.section
                        console.log("WelcomeSTEP: set step and navigate")                        
                        this.props.nextFn()
                    }}
                /> 
            </Content>            
        </Container>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#400d5d', 
    alignItems:'center',
    justifyContent:'center'   
    
  },
  header:{
      color:'#fff',
      fontSize:30,
      fontWeight:'bold'
  },
  subtitle:{
    color:'#fff',
    fontSize:15,
    fontWeight:'bold'
  },
  button:{
      marginTop:50,
      paddingTop:50
  }
});
