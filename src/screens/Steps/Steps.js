import MultiStep from 'react-native-multistep-wizard'
import WelcomeStep from './WelcomeStep';
import React from 'react';
import {View, StyleSheet} from 'react-native';
import AGBStep from './AGBStep';



/* Define the steps of multistep wizard */



export default class Steps extends React.Component{

    /* define the method to be called when the wizard is finished */
    
    constructor(props){
        super(props)
    }

    componentDidMount(){
        console.log('STEPS:'+JSON.stringify(this.props))
    }

    finish(wizardState){
    //code to be executed when wizard is finished
            
    }
    
    /* render MultiStep */
    render(){
        const steps = [
            {name: 'StepOne', component: <WelcomeStep navigation={this.props.navigation}/>},
            {name: 'StepTwo', component: <AGBStep navigation={this.props.navigation}/>}
        ];

        return( 
            <View style={styless.container}>
                <MultiStep steps={steps} onFinish={this.finish}/>
            </View>
        ) 
    }

}

const styless = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#400d5d',
    },
});
  