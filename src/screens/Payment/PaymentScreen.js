
//make this component available to the app
import React, { Component } from 'react';
import { View, Text, Image, StyleSheet,KeyboardAvoidingView } from 'react-native';
import { WebView } from 'react-native-webview';
import {store} from '../../store/store'
import {observer} from 'mobx-react'
import { Spinner } from 'native-base';
import {Auth} from 'aws-amplify'

// create a component
@observer
class PaymentScreen extends Component {

    constructor(props){
        super(props)               
    }

    async componentDidMount(){
        console.log("PAYMENT_MOUNT")
      
        await store.paymentStore.getPaypalView(store.eventStore.activeEvent)
       
    }

    render() {
        return (  
            <KeyboardAvoidingView style={styles.container} behavior="padding">
                <WebView v-if={store.paymentStore.uri != null && store.paymentStore.capturing}               
                 source={{uri:store.paymentStore.uri, headers:{
                     Authorization:'Bearer ' + store.paymentStore.token
                 }}} 
                 javaScriptEnabled={true}
                 originWhitelist={['*']}
                 domStorageEnabled={true}
                 thirdPartyCookiesEnabled={true}
                 onError={(err)=>console.log(err)}                                  
                 onMessage={()=>this._return()}            
                 renderLoading={()=> <Spinner/>}
                    
                />
                {store.paymentStore.capturing &&
                        
                         <Spinner  />
                }
                

                </KeyboardAvoidingView>          
        );
    }

    async _return(){
       
        await store.paymentStore.capture(store.eventStore.activeEvent)
        this.props.navigation.navigate('App')
    }
 
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 15,
      backgroundColor: '#fff',
    },
  });


  
//make this component available to the app
export default PaymentScreen;

  
  

 
  
  