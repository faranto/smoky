import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import * as Location from 'expo-location'
import MapView from 'react-native-maps'
import * as Permissions from 'expo-permissions'
import {store} from '../../store/store'
import { Button, Card, CardItem } from 'native-base';

export default class MapScreen extends React.Component {
  static navigationOptions = {
    title: 'Map',
  }; 

  constructor(props){
      super(props) 
      this.state = {mounted:false,map:null, location:null,errorMessage:null, markers:[]}   
  }

  componentWillMount(){
       this._getLocationAsync();   
       Location.watchHeadingAsync(null, function(coords){
          this.setState({location:coords})
       })     
  }

  componentDidMount(){
    this.setState({mounted:true})
    let {lat,long} = store.eventStore.activeEvent.coords;
    let LatLng = {
       latitude:lat,
       longitude:long
     }
     this.setState({markers:[LatLng]})
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }
    let locationState = await Location.getProviderStatusAsync()
    if(locationState.locationServicesEnabled && locationState.gpsAvailable){
      let location = await Location.getCurrentPositionAsync({});
      this.setState({ location:JSON.stringify(location) });
    }else {
      this.setState({location:'Aktiviere deine Standortinformationen'})
    }
    
  };
  
  render() { 

     
      let {lat,long} = (store.eventStore.activeEvent.coords);
      let LatLng = {
         latitude:lat,
         longitude:long
       }
      let markers = []

      var rnd = function getRandomArbitrary() {
        return Math.random() * (0.03 - 0) + 0;
      }
      var mix = function(){
        markers = [];
        for(var i = 0; i< 6;i++){
          let x = rnd()
          let y = rnd();
          if(x*x + y*y < 0.1){
            markers.push({
              latitude:lat + x,
              longitude:long + y
            })
          }          
        }
        return markers;
    }    
      
      
    return (

      <ScrollView contentContainerStyle={{flex:1}}>  
      <Card style={{flex:1}}> 
        <CardItem>
          <Text>Location of {store.eventStore.activeEvent.place}</Text>
        </CardItem>
        <CardItem style={{flex:1}}>
          <View style={{flex:1}}>
        <MapView 
        style={{ flex: 1}}
        initialRegion={{
          latitude: lat,
          longitude: long,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }} 
      > 
      { this.state.markers.map((x,i) => {
            return <MapView.Marker
                    key={i}
                    coordinate={x}
                    title={store.eventStore.activeEvent.text}
                    description={store.eventStore.activeEvent.place}
                /> 
        })
      
      }       
      </MapView>  
      </View>
      </CardItem>
     {/* <CardItem>
      <Button style={{flex:1}} onPress={()=> {
          let markers = mix()
          this.setState({markers:markers})
          }}><Text>Shuffle</Text></Button> 
        </CardItem>
        */}
      </Card>
     
      </ScrollView>               
    );
  }
}

