import React from 'react';
import { Card, CardItem, Text, Left, Body, Right } from 'native-base';
import {
    StyleSheet,   
    View,
  } from 'react-native';
import {store} from '../../store/store'
import { observer } from 'mobx-react';
import { CheckBox } from 'react-native-elements';

export default class AttendeeCard extends React.Component {
  static navigationOptions = {
    title: 'Details',
  };
  
  render() {
        <View>
             <Card key={i}>
                    <CardItem>
                      <Body>
                       <Text>{a.nickname} </Text>
                      </Body>  
                    </CardItem>
                    <CardItem>
                    <Left>
                         <CheckBox checked={this.props.a.payed} title="payed"  />                       
                      </Left>
                      <Right>                    
                        <CheckBox checked={a.present} title="present" onPress={(x)=> {
                        
                         store.eventStore.attendees =  store.eventStore.attendees.map((a)=> {
                       
                           if(a.userId === x.userId) {
                             if(x.present){
                                a.present = false;
                             } return x;                             
                           }else {                            
                             return a;
                           }
                         })
                         } } /> 
                      </Right>
                    </CardItem>
                  </Card>
        </View>  
   
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  card:{
    padding:10
  }

});