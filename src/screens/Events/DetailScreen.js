import React from 'react';
import { Container, Header,ListItem,List, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import Markdown from 'react-native-markdown-renderer';
import {
    Image,
    ScrollView,
    StyleSheet
  } from 'react-native';
import {store} from '../../store/store'
import { observer } from 'mobx-react';

@observer
export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Details',
  };

  constructor(props){
    super(props);  
  }


  componentWillMount(){
    let eventId = store.eventStore.activeEvent.eventId;   
  }  
  
  render() {
    let newsContent ='Keine Details vorhanden'
   
    if(store.eventStore.activeEvent.type === 'news')
       newsContent = store.eventStore.activeEvent.newscontent;
    else
       newsContent = store.eventStore.activeEvent.detailsMarkdown;
    return (
        <ScrollView style={styles.container}>
        <Card style={styles.card}> 
            <CardItem>
              <Left>
                <Thumbnail source={{uri: "https://d2tfsilug5n0n4.cloudfront.net/" + store.eventStore.activeEvent.image + ".png" }} />
                <Body>
                  <Text>{store.eventStore.activeEvent.text}</Text>
                  <Text note>{store.eventStore.activeEvent.date}</Text>
                </Body>
              </Left> 
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: "https://d2tfsilug5n0n4.cloudfront.net/" + store.eventStore.activeEvent.image + ".png"}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem cardBody>
                 <Body>
                 <Markdown>{newsContent}</Markdown>                                 
                </Body>                
            </CardItem>           
            
          </Card>
          </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  card:{
    padding:10
  }

});