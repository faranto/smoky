
//make this component available to the app
import React, { Component } from 'react';
import { StyleSheet,KeyboardAvoidingView } from 'react-native';

import { Authenticator, SignIn, ConfirmSignUp, SignUp } from 'aws-amplify-react-native';
import CustomConfirmation from './CustomConfirmation'
// create a component
class SignUpScreen extends Component {
    
    render() {
        return (  
            <KeyboardAvoidingView style={styles.container} behavior="padding">
     
                <Authenticator hideDefault={true} authState='signUp' 
                    
                >
                    <SignUp signUpConfig={
                    {
                        hideAllDefaults: ["phone_number","username","email"],
                        defaultCountryCode: '1',
                        signUpFields:[
                            {label:"Email",key:"username"},
                            {label:"Name",key:"name"},
                            {label:"Family name",key:"family_name"},
                            {label: 'Password',key: 'password',type: 'password'}
                        ]                    
                    }}  />
                    <CustomConfirmation override={ConfirmSignUp}{...this.props} />
                           
                </Authenticator> 
            </KeyboardAvoidingView>
                
            
        );
    }
 
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 15,
      backgroundColor: '#fff',
    },
  });
  
//make this component available to the app
export default SignUpScreen;

  
  

 
  
  