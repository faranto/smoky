import React from 'react';
import {
  ActivityIndicator,
  StatusBar,
  View,
} from 'react-native';
import {store} from '../../store/store'
export default class AuthLoadingScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    try {
      await store.authStore.checkCurrentUser()      
    } catch(e){
      console.log(e)         
    } finally{
      this.props.navigation.navigate("App")
    }
    
  };

  // Render any loading content that you like here
  render() {
    return (
      <View >
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}