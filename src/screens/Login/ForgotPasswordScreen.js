
//make this component available to the app
import React, { Component } from 'react';
import { View, Text, Image, StyleSheet,KeyboardAvoidingView } from 'react-native';
import ForgotPasswordForm from './ForgotPasswordForm';
import {store} from '../../store/store'

// create a component
class ForgotPasswordScreen extends Component {

    async componentDidMount(){
        try {
            await store.authStore.checkCurrentUser()
            if(store.authStore.loggedIn){
                this.props.navigation.navigate("App")
            }
        }catch(e){
            console.log(e)
        }        
    }

    render() {
        return (
        <KeyboardAvoidingView behavior="padding" style={styles.container}>             
               
                   <ForgotPasswordForm navigation={this.props.navigation} />
             
            </KeyboardAvoidingView>
        );
    }
 
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    loginContainer:{
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width:150,
        height:150
    },
    logo2: {
        position: 'absolute',
        width:300,
        height: 200
    },
    title:{
        color: "#FFF",   
        textAlign: 'center',
        opacity: 1
    }
});

//make this component available to the app
export default ForgotPasswordScreen;

  
  

 
  
  