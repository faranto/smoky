import React, { Component } from 'react';
import { View, Text, TextInput,Linking, TouchableOpacity, Alert, Button ,StyleSheet ,StatusBar} from 'react-native';
import {Spinner} from 'native-base'
import {store} from '../../store/store'
import { observable } from 'mobx';
import {Authenticator,SignIn,ForgotPassword, SignUp} from 'aws-amplify-react-native'
import { Hub } from 'aws-amplify';

// create a component
class ForgotPasswordForm extends React.Component {
   
    constructor(props){
        super(props)  
        this.state = {username:'',password:'', isLoading:false, error:false}
        Hub
    }
    
    render() {
        var loginButton = null; 
        let error = null;
        if(this.state.isLoading){
           loginButton =   <Spinner style={styles.buttonContainer} />
        }else {
            loginButton =    <TouchableOpacity  style={styles.buttonContainer} onPress={this._forgot}>
                                         <Text  style={styles.buttonText}>submit</Text>
                                    </TouchableOpacity>;
        }

        if(this.state.error != false){
           error = <Text style={styles.error}> {this.state.error}</Text>
        }else {
            error = null
        }

        return (
          
             <Authenticator hide={[SignIn]} authState='forgotPassword' >
              
             </Authenticator> 
                        
             
          
        );
    }

    

    _forgot = async () => {
       this.setState({isLoading:true})
       try{
             let ret = await store.authStore.forgotPassowrd(this.state.username)
             console.log(ret)
       }catch(err){
           console.log(err)
            this.state.error = err.message
            this.setState({isLoading:false})

       }
        
    };


}

// define your styles
const styles = StyleSheet.create({
    error:{
        color: 'red',
        fontSize:20
    },
    input:{
        height: 60,
        backgroundColor: 'rgba(255,255,255,1.0)',
        marginBottom: 10,
        padding: 10,
        color: 'black',
        marginLeft: 10,
        marginRight: 10
    },
    buttonContainer:{
        backgroundColor: '#2980b6',
        padding:10,
        marginBottom: 5,
        marginLeft: 5,
        marginRight: 5
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }, 
    loginButton:{
      backgroundColor:  '#2980b6',
       color: '#fff'
    }
   
});

//make this component available to the app
export default ForgotPasswordForm;