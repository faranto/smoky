import React, { Component } from 'react';
import { View, Text, TextInput,Linking, TouchableOpacity, Alert, Button ,StyleSheet ,StatusBar} from 'react-native';
import {Spinner} from 'native-base'
import {store} from '../../store/store'
import { observable } from 'mobx';


// create a component
class LoginForm extends React.Component {
   
    constructor(props){
        super(props)  
        this.state = {username:'',password:'', isLoading:false, error:false}
    }
    
    render() {
        var loginButton = null; 
        let error = null;
        if(this.state.isLoading){
           loginButton =   <Spinner style={styles.buttonContainer} />
        }else {
            loginButton =    <TouchableOpacity  style={styles.buttonContainer} onPress={this._signInAsync}>
                                         <Text  style={styles.buttonText}>LOG IN</Text>
                                    </TouchableOpacity>;
        }

        if(this.state.error != false){
           error = <Text style={styles.error}> Falsche Zugangsdaten</Text>
        }else {
            error = null
        }

        return (
            <View style={styles.container}> 
                        {error}
                <TextInput style = {styles.input} 
                            autoCapitalize="none" 
                            onSubmitEditing={() => this.passwordInput.focus()} 
                            autoCorrect={false} 
                            keyboardType='email-address' 
                            returnKeyType="next" 
                            placeholderTextColor='grey'
                            value={this.state.username}
                            onChangeText={(username) => this.setState({username})}
                            placeholder='Email' 
                          />

                <TextInput style = {styles.input}   
                           returnKeyType="go" ref={(input)=> this.passwordInput = input} 
                           placeholder='Password' 
                           placeholderTextColor='grey' 
                           value={this.state.password}
                           onChangeText={(password) => this.setState({password})}
                           secureTextEntry/>
                 {/*   <Button onPress={onButtonPress} title = 'Login' style={styles.loginButton} /> */}
                 
                        {loginButton}
                        <TouchableOpacity  style={styles.buttonContainer} onPress={()=>{
                            this.props.navigation.navigate('SignUpScreen')  
                            }
                            }>
                                         <Text  style={styles.buttonText}>SIGN UP</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style={styles.buttonContainer} onPress={()=>{
                            this.props.navigation.navigate('ForgotPasswordScreen')  
                            }
                            }>
                                         <Text  style={styles.buttonText}>Forgot Password</Text>
                        </TouchableOpacity>
             
            </View>
        );
    }

    

    _signInAsync = async () => {
       this.setState({isLoading:true})
     
       store.authStore.getAuthenticated(this.state.username,this.state.password)
        .then(()=>{
                this.props.navigation.navigate('App')
            })          
        .catch((err)=>{         
            this.setState({isLoading:false,error:err})
            setTimeout(()=>{
                this.setState({error:false})
            },2000)

        })
        
    };
}

// define your styles
const styles = StyleSheet.create({
    error:{
        color: 'red',
        fontSize:20
    },
    input:{
        height: 60,
        backgroundColor: 'rgba(255,255,255,1.0)',
        marginBottom: 10,
        padding: 10,
        color: 'black',
        marginLeft: 10,
        marginRight: 10
    },
    buttonContainer:{
        backgroundColor: '#2980b6',
        padding:10,
        marginBottom: 5,
        marginLeft: 5,
        marginRight: 5
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }, 
    loginButton:{
      backgroundColor:  '#2980b6',
       color: '#fff'
    }
   
});

//make this component available to the app
export default LoginForm;