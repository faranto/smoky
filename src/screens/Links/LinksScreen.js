import React from 'react';
import { ScrollView, StyleSheet , Button, Text} from 'react-native';
import  ProfilHead  from '../../components/ProfilePage/ProfilHead';
import {store} from '../../store/store'
import ProfilESN from '../../components/ProfilePage/ProfilESN';
import ProfilBuddy from '../../components/ProfilePage/ProfilBuddy';
export default class LinksScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        {/* Go ahead and delete ExpoLinksView and replace it with your
           * content, we just wanted to provide you with some helpful links */}
        { store.authStore.loggedIn &&
          <ProfilHead/>          
        }
        {store.authStore.loggedIn &&
             <ProfilESN/>
        }
        

      
      
      </ScrollView>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
