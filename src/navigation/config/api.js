

const eventAPI = (mode)=>{
    if(mode === 'prod')
        return events_prod
    else{
        return events_dev
    }
}

const sectionAPI = (mode) => {
    if(mode === 'prod')
        return section_dev
    else{
        return section_dev
    }
}

const paymentAPI = (mode) => {
    if(mode === 'prod')
        return payment_prod
    else{
        return payment_dev
    }
}

const feedbackAPI = (mode) => {
    if(mode === 'prod')
        return feedback_prod
    else{
        return feedback_dev
    }
}





const events_prod = {
    EVENTS_GET:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/events',
    ATTENDANCE_SAVE_S3:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/events/attend/save',
    EVENTS_POOR:'https://d2tfsilug5n0n4.cloudfront.net/events.json',
    IMG:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/img/',
    EVENTS_ATTEND:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/events/attend/',
    PAYMENT_RESERVE:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/payment/reserve',
    ATTEND_RANDOM:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/events/attend/random',   
    EVENTS_ATTENDED:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/events/attend/attended'
    
}

const events_dev = {
    EVENTS_GET:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/events',
    ATTENDANCE_SAVE_S3:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/events/attend/save',
    EVENTS_POOR:'https://d2tfsilug5n0n4.cloudfront.net/events.json',
    IMG:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/img/',
    EVENTS_ATTEND:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/events/attend/',
    PAYMENT_RESERVE:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/payment/reserve',
    ATTEND_RANDOM:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/events/attend/random',   
    EVENTS_ATTENDED:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/events/attend/attended'

}

const payment_prod = {
    PAYMENT_CAPTURE:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/payment/capture',
    PAYMENT_PAY: (eventId)=> { return 'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/payment/pay?eventId='+eventId+'&userId='}
}

const payment_dev = {
    PAYMENT_CAPTURE:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/payment/capture',
    PAYMENT_PAY: (eventId)=> { return 'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/payment/pay?eventId='+eventId+'&userId='}
}


const section_dev = {
    CHANGE_SECTION: 'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/user/changesection'
}


const feedback_prod = {
    FEEDBACK_GET_ACTIVE:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/feedback/active',
    FEEDBACK_SUBMIT:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/feedback'
}

const feedback_dev = {
    FEEDBACK_GET_ACTIVE:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/feedback/active',
    FEEDBACK_SUBMIT:'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/dev/feedback'
}

const REST = {eventAPI, sectionAPI, paymentAPI, feedbackAPI}

export default REST