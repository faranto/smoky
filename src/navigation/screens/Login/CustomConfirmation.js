
//make this component available to the app
import React, { Component } from 'react';
import { View, Text, Image, StyleSheet,KeyboardAvoidingView, Button } from 'react-native';
import {WebView} from 'react-native'
import LoginForm from './LoginForm';
import { Authenticator, SignIn, ConfirmSignUp } from 'aws-amplify-react-native';

// create a component
class CustomConfirmation extends ConfirmSignUp {

    constructor(props){
        super(props)
    }
    
    showComponent(theme) {
      if (this.props.authState === 'confirmSignUp') {
        return (
            <View>
                 <Image 
                source={require('../../assets/images/star.png')}
                resizeMode="contain"
                fadeDuration={0}
                style={{ width: 20, height: 20, marginTop: 1 }}
              />
                <Text>Check your email for a confirmation link.</Text>
                <Button title='Go to Login Screen' onPress={()=> {
                    this.props.navigation.navigate('Auth')
                }}></Button>
          </View>
        );
      } else {
        return null;
      }
    }
}







const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 15,
      backgroundColor: '#fff',
    },
  });
  
//make this component available to the app
export default CustomConfirmation;

  
  

 
  
  
