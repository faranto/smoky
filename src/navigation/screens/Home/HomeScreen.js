import React from 'react';
import {observer} from 'mobx-react'
import {
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
  View,
  Button
} from 'react-native';
import {Spinner, Text} from 'native-base'

import {store} from '../../store/store'
import EventItem from '../../components/EventView/EventItem'
import Roboto from '../../assets/fonts/Roboto.ttf' // './fonts/Roboto.ttf';
import Roboto_medium from '../../assets/fonts/Roboto_medium.ttf';
import * as Font from 'expo-font'
import AuthStore from '../../store/AuthStore';


@observer
export default class HomeScreen extends React.Component {
  
  static navigationOptions = {
    header: null,
  }; 


  async componentDidMount(){  
    await store.notification.activateNotifications()
    if(store.authStore.loggedIn){           
      try{ 
        await store.eventStore.getEvents()   
      } catch(e){
        //error while fetching events
        console.log(e)
      }
    } else {
      await store.eventStore.getPoorEventsTimeout()
    }
  }

  async componentWillMount(){
    await Font.loadAsync({
      Roboto,
      Roboto_medium
    }); 

    this.setState({ fontLoaded: true });   
  }

  constructor(props){
    super(props)
    this.state = {refreshing:false, fontLoaded:false} 
  }

   
  render() {  
    let eventList = null;
    if(!store.eventStore.loading){
        eventList = store.eventStore.events.map((x,i) =>{ return <EventItem key={i} event={x} navigation={this.props.navigation} loggedIn={true} />})
    }
    return (      
      <View style={styles.container}> 
            
        <ScrollView 
          style={styles.container}  
          contentContainerStyle={styles.contentContainer}
          refreshControl={ 
            <RefreshControl  
              refreshing={this.state.refreshing}            
              onRefresh={this._refresh}
            />}>
            {(!store.authStore.loggedIn  && this.state.fontLoaded) &&
              <Button title="login to register for an event"  onPress={this._login} >               
              </Button> 
            }

            {(store.authStore.loggedIn && this.state.fontLoaded) ?
              <Button title="Send us Feedback for our last Event" onPress={this._feedback} >              
              </Button> : null
            }


            {  (!store.authStore.loggedIn) ?
                   
                    store.eventStore.events.map((x,i)=> { return <EventItem
                      key={i}
                      event={x}
                      navigation={this.props.navigation}
                      loggedIn={false}
                    />
                })
                : eventList        
            }
          <View style={styles.helpContainer}> 
          { (store.authStore.loggedIn) &&            
            <TouchableOpacity onPress={this._logout} style={styles.helpLink}>
              <Text style={styles.helpLinkText}>LOGOUT</Text>              
            </TouchableOpacity>
          }
            {store.eventStore.loading ?
                <Spinner style={styles.buttonContainer} />
                :       <Text style={styles.swipe}>Swipe down to refresh Events</Text>
            }
          </View> 
        </ScrollView>        
      </View>
    );
  }

 

 

  _logout = async () => {    
    store.authStore.logout()
    .then(()=> {      
      this.props.navigation.navigate('App');
    });
  };

  _login = async () => {           
    this.props.navigation.navigate('Auth')
  };

  _feedback = async () => {
    this.props.navigation.navigate('FeedbackScreen')
  }

  _refresh = async () => {
    this.setState({refreshing: true});
    if(store.authStore.loggedIn){
      try {      
        await store.eventStore.getEvents() 
      }catch(e){
        await this._logout();
      }
      this.setState({refreshing: false});      
    }else {
      await store.eventStore.getPoorEventsTimeout()
      try {
        await store.authStore.checkCurrentUser()
      }catch (e) {
        await this._logout()
      }    
      this.setState({refreshing: false});
    }
  }

}

const styles = StyleSheet.create({
  swipe:{
    color:'black',
    fontWeight:'bold'
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
