import React from 'react';
import { Form,Textarea, Card, CardItem, Text, Button, Left, Body, Right } from 'native-base';

import {
    ScrollView,
    StyleSheet,  
  } from 'react-native';

import { observer } from 'mobx-react';

import Roboto from './fonts/Roboto.ttf';
import Roboto_medium from './fonts/Roboto_medium.ttf';
import * as Font from 'expo-font'
import {store} from '../../store/store'


@observer
export default class FeedbackScreen extends React.Component {
  static navigationOptions = {
    title: 'Feedbacks',
  };


  constructor(props){
    super(props);
   
  }

  survey = 'No Active Surveys available'
  async componentWillMount(){
    await Font.loadAsync({
        Roboto,
        Roboto_medium
    }); 

    await store.feedbackStore.getFeedbacks()
  }
  
  render() {
    let feedbacks = store.feedbackStore.feedbacks
   
    
    return (
        <ScrollView style={styles.container}>
         {feedbacks.length === 0 &&
              <Text>{this.survey}</Text>
         }
         
          {feedbacks.map((survey,i)=> {
                return (
                    <Card key={i}>
                        <CardItem header >
                        <Left>              
                            <Body>                           
                                <Text style={{fontWeight:'bold'}}>{survey.text}</Text>
                            </Body>
                        </Left>
                        <Right>
                            <Button onPress={()=> {
                                survey.ratings = {}
                                store.feedbackStore.activeSurvey = survey

                                this.props.navigation.navigate('FeedbackAbgabe')
                            }}>
                                <Text>Send Feedback</Text>
                            </Button>
                        </Right>
                       
                        </CardItem>       
                    
                    </Card>

                )
          })}  
          
          </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  card:{
    padding:10
  }

});