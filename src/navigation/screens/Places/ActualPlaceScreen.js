import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import * as Location from 'expo-location'
import MapView from 'react-native-maps'
import * as Permissions from 'expo-permissions'
import {store} from '../../store/store'
import { Button, Card, CardItem } from 'native-base';

export default class PlacesScreen extends React.Component {
  static navigationOptions = {
    header: null,
  }; 


  constructor(props){
      super(props) 
      this.state = {mounted:false,map:null, location:null,errorMessage:null, markers:[]}   
  }

  componentWillMount(){
       this._getLocationAsync();   
       Location.watchHeadingAsync(null, function(coords){
          this.setState({location:coords})
       })     
  }

  componentDidMount(){
    this.setState({mounted:true})
    
    let LatLng = {
       title:'HTW Dresden',
       description:'Your University :)',
       latitude:51.0371558,
       longitude:13.7347729
     }
     this.setState({markers:[LatLng]})
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }
    let locationState = await Location.getProviderStatusAsync()
    if(locationState.locationServicesEnabled && locationState.gpsAvailable){
      let location = await Location.getCurrentPositionAsync({});
      this.setState({ location:JSON.stringify(location) });
    }else {
      this.setState({location:'Aktiviere deine Standortinformationen'})
    }
    
  };
  
  render() {
    return (

      <ScrollView contentContainerStyle={{flex:1}}>  
      <Card style={{flex:1}}> 
        <CardItem style={{flex:1}}>
          <View style={{flex:1}}>
        <MapView 
        style={{ flex: 1}}
        initialRegion={{
          latitude: 51.0371558,
          longitude: 13.7347729,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }} 
      > 
      {store.orientationStore.actualPlace ? 
         <MapView.Marker
            key={0}
            coordinate={store.orientationStore.actualPlace.coords}
            title="Your Destination"
            description=""
        />   
      
      
        : this.state.markers.map((x,i) => {
            return <MapView.Marker
                    key={i}
                    coordinate={x}
                    title={x.title}
                    description={x.description}
                /> 
            
        })
      
      }       
      </MapView>  
      </View>
      </CardItem>
     {/* <CardItem>
      <Button style={{flex:1}} onPress={()=> {
          let markers = mix()
          this.setState({markers:markers})
          }}><Text>Shuffle</Text></Button> 
        </CardItem>
        */}
      </Card>
     
      </ScrollView>               
    );
  }
}

