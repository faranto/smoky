import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/Home/HomeScreen';
import LinksScreen from '../screens/Links/LinksScreen';
import DetailScreen from '../screens/Events/DetailScreen'
import PlacesScreen from '../screens/Places/PlacesScreen'
import ActualPlaceScreen from '../screens/Places/ActualPlaceScreen'
import MapScreen from '../screens/Places/MapScreen'
import FeedbackScreen from '../screens/Feedback/Feedback'
import FeedbackAbgabe from '../screens/Feedback/FeedbackAbgabe'
//import AdminScreen from '../screens/Admin/AdminScreen';
//import AttendanceScreen from '../screens/Admin/AttendanceScreen';
//import AdminRegisterScreen from '../screens/Admin/AdminRegisterScreen';


  
    const HomeStack = createStackNavigator({
      Home: HomeScreen,
      DetailScreen: DetailScreen,
      ActualPlaceScreen: ActualPlaceScreen,
      MapScreen:MapScreen,
      FeedbackScreen:FeedbackScreen,
      FeedbackAbgabe:FeedbackAbgabe
    })
    
    HomeStack.navigationOptions = {
      tabBarLabel: 'Events',
      tabBarIcon: ({ focused }) => (
        <TabBarIcon
          focused={focused}
          name={
            Platform.OS === 'ios'
              ? `ios-information-circle${focused ? '' : '-outline'}`
              : 'md-information-circle'
          }
        />
      ),
    };
    
    const LinksStack = createStackNavigator({
      Links: LinksScreen,
    });
    
    LinksStack.navigationOptions = {    
      tabBarLabel: 'Profile',
      tabBarIcon: ({ focused }) => (
        <TabBarIcon
          focused={focused}
          name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
        />
      ),
    };
    

    const PlacesStack = createStackNavigator({
      Places: PlacesScreen,
    });
    
    PlacesStack.navigationOptions = {
      tabBarLabel: 'Places',
      tabBarIcon: ({ focused }) => (
        <TabBarIcon
          focused={focused}
          name={Platform.OS === 'ios' ? 'ios-map' : 'md-map'}
        />
      ),
    };
    

 





export default createBottomTabNavigator({
  HomeStack,
  PlacesStack,
  LinksStack
})


