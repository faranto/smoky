import axios from 'axios';
import { observable, action } from 'mobx';
import {Buffer} from 'buffer'
import {Auth, API,graphqlOperation} from 'aws-amplify'
import REST from '../config/api'

export default class EventStore{
/*
  mock = {
            "date": "2019-10-30",
             "detailsMarkdown": "w",
             "eventId": "797b2340-904f-11e9-aa64-13f289763e08",
             "image": "5648bb10-6009-11e9-ba5b-df480709d9f9",
             "price": 1.00,
             "slots": "22",
             "starttime": "09:00",
             "text": "Get2Know Leipzig",
             "time": null,
             "type": "free_event"
    }

    mock2 = {
        "date": "2019-10-30",
        "text": "Lunch Time with Faranto",
        "type":"news_event"
    }*/
    err = {
         "text": "Error Loading Events... try again",
         "time": null,
         "type": "free_event"
    } 
     
    @observable loading = false;
    @observable attendLoading = false;
    @observable events
    @observable activeEvent
    @observable attendees;
    
    @observable fullSlots = false;
  

    constructor(root,api){
        this.root = root; 
        this.API = REST.eventAPI(api)
        this.events = [] 
        this.attendees = [{"nickname":""}]
    }

    timeout(ms, promise){
        let timeout = new Promise((resolve,reject)=> {
            let id = setTimeout(()=> {
                clearTimeout(id)                             
                reject({data:[this.err]})            
            },ms);
        })
 
        return Promise.race([
            promise,
            timeout
        ])
    }

    async getEvents(){
        try {
            this.loading = true;
            let events = await this.timeout(3000, this.getEventsTimeout())
            this.events = events
            console.log(events)
            //this.events = [this.mock,this.mock2,this.mock2,this.mock2,this.mock2]
            this.loading = false;
        }catch(err){
            console.log(err)
            this.events = err.data
            this.loading = false;
            return false;
        }    
    }

    @action
    getEventsTimeout(){
        return new Promise( async (resolve,reject)=>{                   
            let url = this.API.EVENTS_GET
            axios.get(url).then( x=> { 
                console.log("EVENTS_GET" + JSON.stringify(x)) 
//resolve([this.mock])
                resolve(x.data.Items)
            })
            .catch((e)=> {
                console.log(e)
                reject(-1)
            })            
         })  
    }

    async saveListToS3(event){
        return new Promise((resolve,reject)=>{ 
            let url = this.API.ATTENDANCE_SAVE_S3
            axios.post(url,{"event":event}).then(x=>{
                resolve(x);
            })
            .catch((err)=>{
                reject(err)
            })

        })
    }

    @action
    getPoorEvents(){ 
       return new Promise( async (resolve,reject) => {
        let url = this.API.EVENTS_POOR
     
        console.log("EVENT_STORE:loading_poor_events")
            try{
                let ret = await axios.get(url)
                    
                console.log("EVENT_STORE:poor_events_loaded")    
                resolve(ret)
            }catch(err){
                console.log(err)
            }
       })
    }
    

    @action
    async getPoorEventsTimeout(){
        try{
            this.loading = true          
            let ret = await this.timeout(3000,this.getPoorEvents()) 
            this.loading = false;
            this.events = ret.data;            
            //this.events = [this.mock,this.mock2,this.mock2,this.mock2,this.mock2]          
            return ret;
        }catch(err){           
            this.events = err.data
            this.loading = false;
            return false;
        }         
    }

    async getPicture(event){         
        
        let url = this.API.IMG + event.eventId
        axios.get(url).then(x=>{           
            var imgBase64 = new Buffer(x.data, 'binary').toString('base64');
            var src = 'data:' + 'image/png' + ';base64, ' + imgBase64;           
            event.imgBase64 = src;
            this.events = this.events.map(x => {
                if(x.eventId === event.eventId){
                    return event;
                }else{
                    return x
                }
            })          
        }).catch(err => console.log(err))
       
    }
 
    async attendEvent(event){
       
        this.attendLoading = true
        axios.post(this.API.EVENTS_ATTEND,{"event":event}).then(x=>{

            console.log("EVENT_STORE:attend_event" + JSON.stringify(event))
            if(x.data.error === undefined){
                event.attendanceStatus = true
                this.events = this.events.map((ev)=> {
                    if(ev.eventId === event.eventId){                        
                        return event; 
                    }                     
                     else return ev
                })   
                this.attendLoading = false;
            }else{
                fullSlots = true
                event.slots = 0
                this.events = this.events.map((ev)=> {
                    if(ev.eventId === event.eventId)
                     return event; 
                     else return ev
                })  
                this.attendLoading = false;
            }                                    
        })
        .catch((err)=>{
            console.log(err)
        })

    }

    async reserveTicket(event){    
       
           
            this.attendLoading = true
            console.log("EVENT_STORE:reserve_ticket"+JSON.stringify(event))
            try{          
                let x = await axios.post(this.API.PAYMENT_RESERVE,{"event":event})                    
            if(x.data.error === undefined){
               this.events = x.data.Items
               this.attendLoading = false;
            }else{
                    fullSlots = true
                    event.slots = 0
                    this.events = this.events.map((ev)=> {
                        if(ev.eventId === event.eventId)
                         return event; 
                         else return ev
                    })  
                    this.attendLoading = false;
            }
        }catch(err){
            console.log(err)
        }                                    
    }
           
       
    
        
     
    addAttendeeToList(user){
        let a = this.attendees 
        a.push(user)
        this.attendees = a
    }

     attendEventRandomUser(random){

        let {event, teilnehmer} = random;
        this.attendLoading = true;
        
        return new Promise((resolve, reject)=> {
        axios.post(this.API.ATTEND_RANDOM,random).then(x=>{    
                this.attendLoading = false       
                resolve(true);
                
            })
            .catch((err)=>{
                this.attendLoading = false
                reject(err)
            })
        })      
    }

    @action
    getAttendees(id){
        
        let url = this.API.EVENTS_ATTEND + id
        return new Promise((resolve, reject)=> {
            axios.get(url).then(x=>{  
                console.log("EVENT_STORE:get_attendees"+x.data.Items)  
                this.attendees = x.data.Items      
                resolve(true);
            })
            .catch((err)=>{
                reject(err)
            })
        })      
    }
    
    @action
    setAttended(eventId,userId,nickname){        
        
        return new Promise((resolve, reject)=> {
            axios.post(this.API.EVENTS_ATTENDED,{event:{
                "eventId":eventId,
                "nickname":nickname,
                "userId":userId
            }
            }).then(x=>{  
                console.log("EVENT_STORE:set_attended"+x.data.Items)  
                this.attendees = x.data.Items      
                resolve(true);
            })
            .catch((err)=>{
                reject(err)
            })
        })      
    }
  
       
}


