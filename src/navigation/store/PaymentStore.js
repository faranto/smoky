import axios from 'axios';
import { observable, action } from 'mobx';
import {Buffer} from 'buffer'
import {Auth, API,graphqlOperation} from 'aws-amplify'
import { FileSystem } from 'expo-file-system';
import REST from '../config/api';

export default class PaymentStore{

 
        
    @observable html = null;
    @observable uri = null;
    @observable token = null;
    @observable capturing = false;

    constructor(root,mode){
        this.root = root; 
        this.API = REST.paymentAPI(mode)
    }


    getPaypalView(eventId){
        return new Promise(async (resolve,reject)=>{  
                this.capturing = true       
                let url = await this.buildUri()
                let session = await Auth.currentSession()                
                this.token = session.getIdToken().getJwtToken()
                
                let x = await axios.get(url)
                
                console.log("PAYMENT:get_paypal_html" + url)
                this.uri = url;
                this.capturing = false;
                   
                          
        })
    }

    async capture(event){ 
       
        //let url = 'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/payment/capture'       
        this.capturing = true
        let evs = this.root.eventStore;
        try{      
          
          let x = await axios.post(this.API.PAYMENT_CAPTURE,{"event":event})
                
        if(x.data.error === undefined){
           evs.events = x.data.Items
           this.capturing = false;
        }else{
                fullSlots = true
                event.slots = 0
                evs.events = this.events.map((ev)=> {
                    if(ev.eventId === event.eventId)
                     return event; 
                     else return ev
                })  
                this.capturing = false;
        }
        }catch(err){
            console.log(err)
        }                                    

    }
  

    async buildUri(){
      let eventId = this.root.eventStore.activeEvent.eventId;
      let user = await Auth.currentAuthenticatedUser()
      let userId = await Auth.userAttributes(user)
      console.log("PAYMENT:get_payment_url")
      let url = 'https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/payment/pay?eventId='+eventId+'&userId='+userId[0].Value
      return url
    }
   
       
}


