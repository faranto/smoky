import Amplify, { Auth } from 'aws-amplify';
import aws_exports from './amplify_config';
import { observable } from 'mobx';
import {  Notifications } from 'expo';
import * as Permissions from 'expo-permissions'
import * as SecureStore from 'expo-secure-store';
import Axios from 'axios';


export default class ExpoLinkVM{

    @observable notificationActivated;

    constructor(root){
        this.root = root;
        this.notificationActivated = false;       
    }

    async activateNotifications(){
        const { status: existingStatus } = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
          );
          let finalStatus = existingStatus;
            console.log("NOTIFICATION"+finalStatus)
          // only ask if permissions have not already been determined, because
          // iOS won't necessarily prompt the user a second time.
          if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            console.log(existingStatus)
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
          }
        
          // Stop here if the user did not grant permissions
          if (finalStatus !== 'granted') {
            return;
          }


        
          // Get the token that uniquely identifies this device
          let token = await Notifications.getExpoPushTokenAsync();
          let storeed = await SecureStore.getItemAsync("ExpoToken");
          if(storeed === null){
            console.log("TODO, send tokens to aws: " + token)
            let payload = {"token":token};
            await Axios.post("https://lsz7xamq0a.execute-api.eu-central-1.amazonaws.com/Ready/notification/352d24df-f9e7-49a9-ac58-152797141f9d",payload)
            await SecureStore.setItemAsync("ExpoToken", token)
          }
     
                 
    }
    
}