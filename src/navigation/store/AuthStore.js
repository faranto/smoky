import Amplify, { Auth } from 'aws-amplify';
import aws_exports from './amplify_config';
import { observable } from 'mobx';
import {AsyncStorage} from 'react-native';

export default class AuthStore{

    @observable loggedIn = false;
    @observable error;
    @observable adminRole;

    constructor(root){
        this.root = root; 
        this.status = "NICHT EINGELOGGT"  
        this.loggedIn = false
        this.adminRole = false;   
    }
    /**
     * Creates a Session for a user using aws cognito
     * @param {string} username 
     * @param {string} password 
     */
    async getAuthenticated(username,password){ 

        return new Promise((resolve,reject) => {
            Auth.signIn({
                username:username,
                password:password
            })
            .then((user)=>{
                if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
                    const { requiredAttributes } = user.challengeParam; // the array of required attributes, e.g ['email', 'phone_number']                   
                    console.log("new password")
                   
                } else {
                    this.loggedIn = true                  
                    resolve(true)   
                }
                         
            })
            .catch((err)=>{
                this.error = JSON.stringify(err)
                reject(err)
            })
        })
        
    }


    async forgotPassowrd(username){      
        return new Promise(async (resolve,reject) => {
            try{
                let reset = await Auth.forgotPassword(username)                
                console.log(reset)
                resolve(reset)
            }catch(err){
                reject(err)
            }
        })           
    }
    
    async logout(){
        await AsyncStorage.clear()
        await Auth.signOut();
        this.loggedIn = false;
        this.adminRole = false;
    }
    /**
     * used for checking if an active session is available in aws cognito
     */
    async checkCurrentUser(){
        try {
            let session = await Auth.currentAuthenticatedUser();            
            this.loggedIn = true
        }catch(e){
            this.loggedIn = false;
            reject("No current user")
        }

    }
    
}