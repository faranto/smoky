import { decorate, observable } from "mobx";
import AuthStore from './AuthStore'
import EventStore from './EventStore'
import Teilnehmer from './Teilnehmer'
import Orientation from './OrientationStore'
import Payment from './PaymentStore'
import ExpoLinkVM from "./ExpoLinkVM";
import FeedbackStore from "./FeedbackStore";
class Store{
   
    constructor(){
        let mode = 'prod'
        this.authStore = new AuthStore(this)       
        this.eventStore = new EventStore(this,mode)
        this.teilnehmerStore = new Teilnehmer(this,mode)
        this.orientationStore = new Orientation(this)
        this.paymentStore = new Payment(this,mode)
        this.notification = new ExpoLinkVM(this)
        this.feedbackStore = new FeedbackStore(this,mode)
    }   
}


export const store = new Store();