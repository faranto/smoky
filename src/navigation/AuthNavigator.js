import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import SignInScreen from '../screens/Login/SignInScreen';
import LinksScreen from '../screens/Links/LinksScreen';
import EventItem from '../components/EventView/EventItem';
// Implementation of HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen
// goes here.


export default createSwitchNavigator({ 
    SignIn: SignInScreen,   
 });




