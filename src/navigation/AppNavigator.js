import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import MainTab from './MainTabNavigator';
import AuthStack from './AuthNavigator'
import AuthLoading from '../screens/Login/AuthLoadingScreen'
import SignUpScreen from '../screens/Login/SignUpScreen'
import PaymentScreen from '../screens/Payment/PaymentScreen'
import ForgotPasswordScreen from '../screens/Login/ForgotPasswordScreen'
import Steps from '../screens/Steps/Steps'
export default  createSwitchNavigator({
        // You could add another route here for authentication.
        // Read more at https://reactnavigation.org/docs/en/auth-flow.html
        AuthLoading:AuthLoading,
        App: MainTab,
        Auth:AuthStack,   
        Welcome:Steps,    
        SignUpScreen:SignUpScreen,
        ForgotPasswordScreen: ForgotPasswordScreen,
        PaymentScreen:PaymentScreen
      },{
        initialRouteName: 'App' ,
        backBehavior :"initialRoute"
      })

 