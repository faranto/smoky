import React from 'react';
import { StyleSheet, Image, View, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import { Auth } from 'aws-amplify';
import avatar from './img/avatar_male.png'


export default class ProfilHead extends React.Component {
  
   constructor(props){
        super(props)
        this.state = {username:''}
   }
   
  render() {
    return (
      <View>
         
        <Card>
           <CardItem>
            <Image 
                    source={avatar}  
                    resizeMode='contain'
                    style={{flex:1,width: 100, height: 100, borderRadius: 100/ 2}} 
            />
            
           </CardItem>
            <CardItem>
              <Left>               
                <Body>         
                  <Text>Hello, {this.state.username}</Text>
                  <Text note>Here you'll find all information regarding your smoky account</Text>
                </Body>
              </Left>
            </CardItem>  
                     
            {/*<CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>Register now</Text>
                </Button>
              </Left>              
            </CardItem> */}          
          </Card>  
 
        
      </View>
    );
  }

  async componentDidMount(){
    let user = await Auth.currentAuthenticatedUser()
    this.setState({username:user.attributes.name + ' ' + user.attributes.family_name})
    console.log("PROFILE HEAD: " + JSON.stringify(user))

  }

 
 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12,
  },
  optionIconContainer: {
    marginRight: 9,
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#EDEDED',
  },
  optionText: {
    fontSize: 15,
    marginTop: 1,
  },
});
