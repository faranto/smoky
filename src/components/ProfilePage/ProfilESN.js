import React from 'react';
import { StyleSheet, Image, View, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import { Auth } from 'aws-amplify';
import logo from './img/faranto_logo.jpg'


export default class ProfilESN extends React.Component {
  
   constructor(props){
        super(props)
        this.state = {username:''}
   }
   
  render() {
    return (
      <View>
         
        <Card>
        <CardItem>
            <Image 
                    source={logo}  
                    resizeMode='contain'
                    style={{flex:1,width: 100, height: 100, borderRadius: 100/ 2}} 
            />
            
           </CardItem>
            <CardItem>
              <Left>               
                <Body>
                  <Text>You are part of faranto ESN Dresden</Text>
                  <Text note>you can participate their events</Text>
                </Body>
              </Left>
            </CardItem>           
            {/*<CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>Join now</Text>
                </Button>
              </Left>              
            </CardItem>      */     }
          </Card>         
      </View>
    );
  } 

 
 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12,
  },
  optionIconContainer: {
    marginRight: 9,
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#EDEDED',
  },
  optionText: {
    fontSize: 15,
    marginTop: 1,
  },
});
