import React from 'react';
import { StyleSheet, Image, View, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Ionicons } from '@expo/vector-icons';


import * as WebBrowser from 'expo-web-browser';


export default class ProfilBuddy extends React.Component {
  
   constructor(props){
        super(props)
        this.state = {username:''}
   }
   
  render() {
    return (
      <View>
         
        <Card>
          <CardItem>
              <Left>               
                <Body>
                  <Text>A buddy was found for you!</Text>
                  <Text>name: </Text>
                  <Text>country: </Text>
                  <Text>mail: </Text>
                </Body>
              </Left>
            </CardItem>           
            {/*<CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>Join now</Text>
                </Button>
              </Left>              
            </CardItem>      */     }
          </Card>         
      </View>
    );
  } 

 
 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12,
  },
  optionIconContainer: {
    marginRight: 9,
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#EDEDED',
  },
  optionText: {
    fontSize: 15,
    marginTop: 1,
  },
});
