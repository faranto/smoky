import React from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';


import * as WebBrowser from 'expo-web-browser';


export default class ExpoLinksScreen extends React.Component {
  render() {
    return (
      <View>
        
 
        <TouchableOpacity
          style={styles.option}
        
          onPress={this._handlePressESN}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Image 
                source={require('../../assets/images/favicon.png')}
                resizeMode="contain"
                fadeDuration={0}
                style={{ width: 20, height: 20, marginTop: 1 }}
              />
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>
                ESN Page
              </Text>
            </View>
          </View>
        </TouchableOpacity>

     <TouchableOpacity
          style={styles.option}
        
          onPress={this._handlePressHTW}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Image 
                source={require('../../assets/images/htw_logo.png')}
                resizeMode="contain"
                fadeDuration={0}
                style={{ width: 20, height: 20, marginTop: 1 }}
              />
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>
                HTW Homepage
              </Text>
            </View>
          </View>
        </TouchableOpacity>
       
        <TouchableOpacity
          style={styles.option}
        
          onPress={this._handlePressOpal}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Image 
                source={require('../../assets/images/htw_logo.png')}
                resizeMode="contain"
                fadeDuration={0}
                style={{ width: 20, height: 20, marginTop: 1 }}
              />
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>
                Opal
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        
      </View>
    );
  }

  _handlePressESN = () => {
    WebBrowser.openBrowserAsync('http://faranto.esn-germany.de/');
  };

  _handlePressOpal = () => {
    WebBrowser.openBrowserAsync('https://bildungsportal.sachsen.de/opal/');
  };
 
  
  _handlePressHTW = () => {
    WebBrowser.openBrowserAsync('https://www.htw-dresden.de/de/startseite.html');
  };
 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12,
  },
  optionIconContainer: {
    marginRight: 9,
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#EDEDED',
  },
  optionText: {
    fontSize: 15,
    marginTop: 1,
  },
});
