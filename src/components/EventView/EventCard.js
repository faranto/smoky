import React, { Component } from 'react';
import { Image,StyleSheet, View, Alert } from 'react-native';
import { Container,Spinner, Header, Button, Content,Toast, Card, CardItem, Thumbnail, Text,  Icon, Left, Body, Right } from 'native-base';
import Roboto from './fonts/Roboto.ttf';
import Roboto_medium from './fonts/Roboto_medium.ttf';
import {store} from '../../store/store'
import {observer} from 'mobx-react'
import * as Font from 'expo-font'

@observer
export default class EventCard extends Component {

    constructor(props){
        super(props)
        this.state = {fontLoaded:false, image:false} 
    }
    async componentWillMount() {
      await Font.loadAsync({
        Roboto,
        Roboto_medium
      }); 

      this.setState({ fontLoaded: true });     
    }

    
     _attendEvent = async ()=>{
      await store.eventStore.attendEvent(this.props.event)     
    }

    _showModal = async ()=>{
      this.setState({image:true})
    }

    _navigatePayment(){
      store.eventStore.activeEvent = this.props.event;
      this.props.navigation.navigate('PaymentScreen')
    }

    async _reserviereTicket(){
      await store.eventStore.reserveTicket(this.props.event)
    }
  
  render() {  
    let button = null;
    
    if(this.state.fontLoaded){
     
      let button = null;

     if(this.props.event.type == 'free_event' && this.props.event.slots > 0){      
        let text = "Teilnehmen";  
        if(this.props.event.attendanceStatus && this.props.event.type == 'free_event') 
              text = "Angemeldet"              
              button =  <Button disabled={this.props.event.attendanceStatus}  onPress={this._attendEvent} > 
                        <Icon name='people'></Icon>
                        <Text>{text}</Text>
                    </Button>                   
          } else if(this.props.event.type == 'free_event'){
            text = "Ausgebucht"
            button =  <Button disabled={true}  >
                          <Icon name='people'></Icon>
                          <Text>{text}</Text>
                      </Button>     
          } else if(this.props.event.type == 'payed_event'){
            let event = this.props.event
            if(!event.payment_status){
              button =  <Button onPress={
                async () => { 
                  Alert.alert(
                      'Caution',
                      'This is a limited event. If you participate, we assume that you want to be part of our event. If something comes up, please let an ESN faranto member know.',
                      [
                      {text: 'Attend', onPress: async () => {
                        this._reserviereTicket()                   
                      
                      }},
                      {
                          text: 'Cancel',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel',
                      }
                      ],
                      {cancelable: false},
                  );
                 
              }
                
                
                
                } >
                          <Icon name='people'></Icon>
                          <Text>Reserviere Ticket</Text>
                        </Button>
            }
            if(event.payment_status && event.payment_status === 'reserved'){
              button =  <Button onPress={() => this._navigatePayment()} >
                          <Icon name='people'></Icon>
                          <Text>Bezahlen</Text>
                        </Button>
            }
            if(event.payment_status && event.payment_status === 'payed'){
              button =  <Button disabled>
                          <Icon name='people'></Icon>
                          <Text>Bezahlt</Text>
                        </Button>
            }
      }
       //style={{borderTopWidth:5,borderTopColor:'#5cb85c'}}
    return (  
        <Card >
            <CardItem header style={{backgroundColor:'#8048a0'}}>
              <Left>              
                <Body>
                  <Text style={{fontWeight:'bold', color:'#fff'}}>{this.props.event.text}</Text>
                  <Text note style={{fontWeight:'bold', color:'#fff'}}>{this.props.event.date}</Text>
                </Body>
              </Left>
              <Right> 
                {this.props.event.coords   && 
                  <Button transparent onPress={()=>{
                    store.eventStore.activeEvent = this.props.event
                    this.props.navigation.navigate('MapScreen');
                  }}>
                     <Icon name='map' />
                     <Text>Show Location</Text>
                  </Button>              
                }       

              </Right>
            </CardItem>
                  
            <CardItem  cardBody>
              <Image  resizeMode="contain" style={{flex:1, height: 200, width: undefined}}  source={{uri: "https://d2tfsilug5n0n4.cloudfront.net/" + this.props.event.image + ".png"}}/>
            </CardItem>
            <CardItem>        
            
            
              <Left>
                <Button onPress={()=>{
                    store.eventStore.activeEvent = this.props.event
                    this.props.navigation.navigate('DetailScreen');                    
                  }} transparent>
                  <Icon active name="search" />
                  <Text>Details</Text>
                </Button>
              </Left> 
              {!store.eventStore.attendLoading && this.props.loggedIn  &&
                <Right>
                {button}
                </Right>
              } 
              {store.eventStore.attendLoading &&
                 <Right>
                  <Spinner />
                  </Right>
              }
             
                       
              
            </CardItem>
            
          </Card>
       
  
    );
    }else {
        return null;
    }
  }
}

const styles = StyleSheet.create({


})