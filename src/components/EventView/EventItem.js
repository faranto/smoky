import React, { Component } from 'react';
import { Image,StyleSheet, View } from 'react-native';
import { Container,Spinner, Header, Button, Content,Toast, Card, CardItem, Thumbnail, Text,  Icon, Left, Body, Right } from 'native-base';
import Roboto from './fonts/Roboto.ttf';
import Roboto_medium from './fonts/Roboto_medium.ttf';
import {store} from '../../store/store'
import {observer} from 'mobx-react'
import * as Font from 'expo-font'
import EventCard from './EventCard';
import NewsCard from './NewsCard';

@observer
export default class EventItem extends Component {

    constructor(props){
        super(props)
        this.state = {fontLoaded:false, image:false} 
    }
    async componentWillMount() {
      await Font.loadAsync({
        Roboto,
        Roboto_medium
      }); 

      this.setState({ fontLoaded: true });     
    }

   
  render() {  
    if(this.state.fontLoaded){
      return (    
        <View>
          
          {this.props.event.type != 'news_event' &&
              <EventCard event={this.props.event} navigation={this.props.navigation} loggedIn={this.props.loggedIn} />
          }

          {this.props.event.type == 'news_event' &&
              <NewsCard event={this.props.event} navigation={this.props.navigation} />
          }

        </View>
      
        
    
      );
      }else {
          return null;
      }
  }
}

const styles = StyleSheet.create({


})