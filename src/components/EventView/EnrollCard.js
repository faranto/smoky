import React, { Component } from 'react';
import { Image,StyleSheet, View } from 'react-native';
import { Container,Spinner, Header, Button, Content,Toast, Card, CardItem, Thumbnail, Text,  Icon, Left, Body, Right } from 'native-base';
import Roboto from './fonts/Roboto.ttf';
import Roboto_medium from './fonts/Roboto_medium.ttf';
import {store} from '../../store/store'
import {observer} from 'mobx-react'
import * as Font from 'expo-font'

@observer
export default class EnrollCard extends Component {

    constructor(props){
        super(props)
        this.state = {fontLoaded:false, image:false} 
    }
    async componentWillMount() {
      await Font.loadAsync({
        Roboto,
        Roboto_medium
      }); 

      this.setState({ fontLoaded: true });     
    }

    
   
  
  render() {  
    let button = null;
    
    if(this.state.fontLoaded){

    return (           
          <Card>
            <CardItem header>
              <Left>              
                <Body>
                  <Text style={{fontWeight:'bold'}}>{this.props.event.text}</Text>                 
                </Body>
              </Left>   
              <Right>
                    <Button onPress={async ()=> {
                            await store.sectionStore.enroll()
                    }}>
                        <Text>
                            Enroll
                        </Text>                         
                    </Button>
                </Right>          
            </CardItem>   
                     
          </Card>
    );
    }else {
        return null;
    }
  }
}

const styles = StyleSheet.create({


})