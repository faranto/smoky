import React, { Component } from 'react';
import { Image,StyleSheet, View } from 'react-native';
import { Container,Spinner, Header, Button, Content,Toast, Card, CardItem, Thumbnail, Text,  Icon, Left, Body, Right } from 'native-base';
import Roboto from './fonts/Roboto.ttf';
import Roboto_medium from './fonts/Roboto_medium.ttf';
import {store} from '../../store/store'
import {observer} from 'mobx-react'
import * as Font from 'expo-font'

@observer
export default class NewsCard extends Component {

    constructor(props){
        super(props)
        this.state = {fontLoaded:false, image:false} 
    }
    async componentWillMount() {
      await Font.loadAsync({
        Roboto,
        Roboto_medium
      }); 

      this.setState({ fontLoaded: true });     
    }

    
   
  
  render() {  
    let button = null;
    
    if(this.state.fontLoaded){

    return (           
          <Card>
            <CardItem header style={{backgroundColor:'#8048a0'}}>
              <Left>              
                <Body>
                  <Text style={{fontWeight:'bold', color:'#fff'}}>{this.props.event.text}</Text>
                  <Text note style={{fontWeight:'bold', color:'#fff'}}>{this.props.event.date}</Text>
                </Body>
              </Left>             
            </CardItem>            
          </Card>
    );
    }else {
        return null;
    }
  }
}

const styles = StyleSheet.create({


})