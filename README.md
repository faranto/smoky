# ESN Smoky App
[![Generic badge](https://img.shields.io/badge/Version-2.0.5-<COLOR>.svg)](https://shields.io/)  [![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)

Smoky is meant to be help for incoming students in Dresden and is maintained by esn faranto e.V.

<img src="https://gitlab.com/faranto/smoky/raw/master/src/assets/images/splash.png" alt="drawing" width="200"/>

# Developer Guide
The App is build using React Native and Expo to provide iOS and Android builds without having to deal with Android Studio or XCode.  

## Prerequesites

*  Smoky Account for Testing
*  git
*  [NodeJS](https://nodejs.org/en/)
*  [Expo CLI](https://docs.expo.io/versions/latest/workflow/expo-cli/)
*  [Visual Studio Code](https://code.visualstudio.com/) (optional but highly recommended)
*  [Expo App for iOS or Android for Testing](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=de)

## Run Project
1. Clone this repository
> git clone https://gitlab.com/ericbrandt91/faranto_react_native
2. install dependencies by running the following inside the folder
> npm install
3. start expo
> expo start

## Project Structure
```
src
+-assets/
+-components/
+-config/
+-navigation/
+-screens/
+-store/
App.js
```

main.js is the entry point for expo and registers App.js as the root component

### MobX
Mobx is used as central state management tool and relies heavily on the observer pattern to propagate state changes. 

e.g. by decorating a Component with the @observer decorator and referencing a observable value from the store the component will rerender on changes accordingly

```
//store class
class EventStore{
    @observable eventId; //if this changes, the Homescreen will rerender the Text Component
}
```

```
//HomeScreen.js
@observer
class HomeScreen extends React.Component{
    //...
    
    render(){
        <View>
            <Text>{store.eventStore.eventId}</Text>
        </View>
    }
}
```




